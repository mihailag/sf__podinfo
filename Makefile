# Makefile
TEST_IMAGE:=$(CI_REGISTRY_IMAGE):$(CI_COMMIT_BRANCH)-$(CI_COMMIT_SHORT_SHA)
RELEASE_IMAGE:=$(CI_REGISTRY_IMAGE):latest
REGISTRY_URL:=https://$(CI_REGISTRY)

build-container:
	docker build --pull -t $(TEST_IMAGE) .
	docker push $(TEST_IMAGE)
	docker images

test-container:
	apk add --no-cache curl
	docker pull $(TEST_IMAGE)
	docker run --rm -d --name podinfo $(TEST_IMAGE)
	docker exec podinfo curl -s localhost:9898 | grep -e hostname -e logo -e goos -e goarch -e num_cpu
	docker images
	docker ps

push-container:
	docker pull $(TEST_IMAGE)
	docker tag $(TEST_IMAGE) $(RELEASE_IMAGE)
	docker push $(RELEASE_IMAGE)

deploy-container:
	helm upgrade podinfo ./charts/podinfo --install --values=./charts/podinfo/values.yaml --recreate-pods